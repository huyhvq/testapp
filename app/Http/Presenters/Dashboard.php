<?php
namespace App\Http\Presenters;

use App\User;
use Carbon\Carbon;

class Dashboard
{

    protected $min;

    protected $max;

    public function getList($age)
    {
        if ($age >= 7 && $age <= 12) {
            $this->min = 7;
            $this->max = 12;
        } elseif ($age >= 13 && $age <= 18) {
            $this->min = 13;
            $this->max = 18;
        } elseif ($age >= 19 && $age <= 24) {
            $this->min = 19;
            $this->max = 24;
        } elseif ($age >= 25 && $age <= 35) {
            $this->min = 25;
            $this->max = 35;
        } elseif ($age >= 36 && $age <= 50) {
            $this->min = 36;
            $this->max = 50;
        } elseif ($age > 50) {
            $this->min = 51;
        }

        $minDate = Carbon::today()->subYears($this->min);
        $maxDate = Carbon::today()->subYears($this->max);
        $users = User::whereBetween('dob', [$maxDate, $minDate])->get();
        return $users;
    }
}