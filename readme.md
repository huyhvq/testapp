# Test App

## How to install

* Clone this repo
* Please add Vhost into app/public
* Make sure app/storeage has write permission.
* Run php composer install
* copy .env.example to .env and configuration

Example:
```php
    APP_ENV=local
    APP_DEBUG=true
    APP_KEY=VwCJjmvoeBs6Bnz0DwYuK1ltKgEg9GfC
    
    DB_HOST=127.0.0.1
    DB_DATABASE=your_db_name
    DB_USERNAME=your_db_username
    DB_PASSWORD=your_db_password
    
    CACHE_DRIVER=file
    SESSION_DRIVER=file
    QUEUE_DRIVER=sync
    
    REDIS_HOST=127.0.0.1
    REDIS_PASSWORD=null
    REDIS_PORT=6379
    
    MAIL_DRIVER=log                     //if you use log as your email driver please check in test/storage/logs/laravel.log to see email when you register
    MAIL_HOST=mailtrap.io
    MAIL_PORT=2525
    MAIL_USERNAME=null
    MAIL_PASSWORD=null
    MAIL_ENCRYPTION=null
```

## Database Migration and Seed

* Please run command
```php
php artisan make:migration // Create Db
php artisan db:seed // For create dummy data
```

* Because I start this test late so I don't have time to implement case 4.
* This app has actions:
    register
    login
    forgot password
    logout
You can see routes by using command

```php
php artisan route:list
```